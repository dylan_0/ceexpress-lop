﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using LOP_DatabaseModel.Model;

namespace CEExpress_LOP.ViewModels
{
    public class OrdersViewModel
    {
        public Order Order { get; set; }
        public PickupStop PickupStop { get; set; }
        public DeliverStop DeliverStop { get; set; }
    }
}