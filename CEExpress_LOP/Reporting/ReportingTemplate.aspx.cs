﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;

namespace CEExpress_LOP.Reporting
{
    public partial class ReportingTemplate : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                try
                {
                    //Grab the desired report folder from the request - will tell if it is Management or Client
                    string reportFolder = Request["Class"].ToString();
                    //rvSiteMapping.ShowCredentialPrompts = true;

                    string custNumber = null;

                    //If client - grab the Customer Number
                    if (reportFolder.Equals("Client"))
                    {
                        custNumber = Request["CustNumber"];
                    }
                    

                    rvSiteMapping.Height = Unit.Pixel(Convert.ToInt32(Request["Height"]));
                    rvSiteMapping.ProcessingMode = Microsoft.Reporting.WebForms.ProcessingMode.Remote;

                    rvSiteMapping.ServerReport.ReportServerUrl = new Uri("http://ceexpress-ssrs.canadacentral.cloudapp.azure.com:80/ReportingService"); // Add the Reporting Server URL
                    rvSiteMapping.ServerReport.ReportPath = String.Format("/{0}/{1}", reportFolder, Request["ReportName"].ToString());
                    rvSiteMapping.ServerReport.ReportServerCredentials = new ReportServerCredentials(ConfigurationManager.AppSettings["reportingserver_username"], ConfigurationManager.AppSettings["reportingserver_passwd"], "CEExpress-SSRS");

                    //If Client, pass along the customer number to the report as a paramerter
                    if(custNumber != null)
                    {
                        rvSiteMapping.ServerReport.SetParameters(new Microsoft.Reporting.WebForms.ReportParameter("CustNum", custNumber));
                    }

                    rvSiteMapping.ServerReport.Refresh();
                }
                catch (Exception ex)
                {
                    System.Diagnostics.Debug.WriteLine("! *** -> Error In: (ReportingTemplate) PAGE_LOAD " + ex.Message);

                }
            }

        }
    }
}