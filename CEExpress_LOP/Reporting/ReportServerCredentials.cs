﻿using System.Net;
using System.Security.Principal;
using Microsoft.Reporting.WebForms;

namespace CEExpress_LOP.Reporting
{
    internal class ReportServerCredentials : IReportServerCredentials
    {
        private string UserName;
        private string Password;
        private string Domain;

        public ReportServerCredentials(string uname, string password, string domain)
        {
            this.UserName = uname;
            this.Password = password;
            this.Domain = domain;
        }

        public WindowsIdentity ImpersonationUser
        {
            get
            {
                /* Use default identity.*/
                return null;
            }
        }

        public ICredentials NetworkCredentials
        {
            get {
                string userName = UserName;
                string password = Password;
                string domain = Domain;
                return new NetworkCredential(userName, password, domain);
            }
            
        }

        public bool GetFormsCredentials(out Cookie authCookie, out string userName, out string password, out string authority)
        {
            authCookie = null;
            userName = null;
            password = null;
            authority = null;
            return false;
        }
    }
}