﻿using CEExpress_LOP.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CEExpress_LOP.Controllers
{
    public class ReportsController : Controller
    {

        [Authorize(Roles ="Management")]
        public ActionResult ReportTemplate(string ReportName, string ReportDescription, int Width, int Height, string Class)
        {
            var rptInfo = new ReportSpecs
            {
                ReportName = ReportName,
                ReportDescription = ReportDescription,
                ReportURL = String.Format("../../Reporting/ReportingTemplate.aspx?ReportName={0}&Height={1}&Class={2}", ReportName, Height, Class ),
                Width = Width,
                Height = Height
            };

            return View(rptInfo);
        }

        
        public ActionResult ClientReportTemplate(string ReportName, string ReportDescription, int Width, int Height, string Class, string CustNumber)
        {
            var rptInfo = new ReportSpecs
            {
                ReportName = ReportName,
                ReportDescription = ReportDescription,
                ReportURL = String.Format("../../Reporting/ReportingTemplate.aspx?ReportName={0}&Height={1}&Class={2}&CustNumber={3}", ReportName, Height, Class, CustNumber),
                Width = Width,
                Height = Height
            };

            return View("ReportTemplate", rptInfo);
        }

    }
}