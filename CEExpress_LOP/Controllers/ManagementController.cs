﻿using CEExpress_LOP.App_Start;
using CEExpress_LOP.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CEExpress_LOP.Controllers
{
    [Authorize(Roles = "Management")]
    public class ManagementController : Controller
    {
        private ApplicationUserManager _userManager;

        // GET: Management
        public ActionResult Home()
        {
            return View();
        }

        public ActionResult ClientDetails()
        {
            if(TempData["register"] != null)
            {
                ViewData["register"] = TempData["register"];
            }

            try{
                    _userManager = HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
                    return View(_userManager.Users.ToList());
                }
                catch (Exception)
                {
                    ViewData["error"] = "There was an error in loading the client management page. Please refresh the page or try again later.";
                    return View();
                }
         
        }

        [HttpGet]
        public ActionResult Settings()
        {
            //Check if there is a status
            if(TempData["status"] != null)
            {
                ViewBag.Status = TempData["status"];
                TempData["status"] = null;
            }
            //When Displaying the settings page, pass it the setting values from web.config
            //Check to see if the cid and apikey settings exist and have values
            if(ConfigurationManager.AppSettings["cid"] != null && ConfigurationManager.AppSettings["apikey"] != null)
            {
                ViewBag.cid = ConfigurationManager.AppSettings["cid"];
                ViewBag.apikey = ConfigurationManager.AppSettings["apikey"];
            }
            else
            {
                ViewBag.cid = "Error! - CID Has not yet been set!";
                ViewBag.apikey = "Error! - API Key has not yet been provided!";
            }

            return View(ViewBag);
        }

        [HttpPost]
        public ActionResult UpdateSettings()
        {

            Configuration objConfig = System.Web.Configuration.WebConfigurationManager.OpenWebConfiguration("~");
            AppSettingsSection settings = (AppSettingsSection)objConfig.GetSection("appSettings");

            string cid = Request["cid"];
            string apikey = Request["apikey"];

            string oldCid = settings.Settings["cid"].Value;
            string oldApiKey = settings.Settings["apikey"].Value;

            try
            {
                bool modified = false;
                if (!cid.Equals(oldCid))
                {
                    settings.Settings["cid"].Value = cid;
                    modified = true;
                } 
                
                if(!oldApiKey.Equals(apikey))
                {
                    settings.Settings["apikey"].Value = apikey;
                    modified = true;
                }

                if (modified)
                {
                    objConfig.Save();
                   
                    return RedirectToAction("SettingsChanged");
                }
                TempData["status"] = "No settings were changed.";
            }
            catch (ConfigurationErrorsException)
            {
                TempData["status"] = "There was an error in updating the settings. Please try again later.";
            }

            return RedirectToAction("Settings");

        }

        public ActionResult SettingsChanged()
        {
            TempData["status"] = "The settings were successfully updated!";
            return RedirectToAction("Settings");
        }

        
        public ActionResult Analytics()
        {
            return View();
        }

        public ActionResult Reports()
        {
            return View();
        }
    }
}