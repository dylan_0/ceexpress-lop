﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using CEExpress_LOP.App_Start;
using CEExpress_LOP.Core;
using CEExpress_LOP.Core.Repository;
using CEExpress_LOP.Models;
using CEExpress_LOP.ViewModels;
using LOP_DatabaseModel.Model;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;

namespace CEExpress_LOP.Controllers
{
    [Authorize]
    public class OrdersController : Controller
    {
        private IOrderRepository orderRepo;

        public OrdersController()
        {
            this.orderRepo = new OrderRepository(new CEExpressDBEntities());
        }
        
        [Authorize]
        // GET: Orders
        public ActionResult Index(string sortRequest, string searchString, string currentOrderFilter)
        {

            ViewBag.CurrentSort = sortRequest;

            ViewBag.OrderNumberSort = String.IsNullOrEmpty(sortRequest) ? "orderNum_desc" : "";
            ViewBag.CustNumSort = sortRequest == "Customer Number" ? "custNumber_desc" : "Customer Number";
            ViewBag.CostCenterSort = sortRequest == "Cost Centre" ? "costCenter_desc" : "Cost Centre";
            ViewBag.StatusDateSort = sortRequest == "Status Date" ? "statusDate_desc" : "Status Date";
            ViewBag.StatusSort = sortRequest == "Status" ? "status_desc" : "Status";

            if (searchString != null)
            {
                
            }
            else
            {
                searchString = currentOrderFilter;
            }

            ViewBag.CurrentFilter = searchString;


            //Clients can see their history of orders placed.
            return View(orderRepo.GetCustOrdersWithSortRequest(sortRequest, GetCustNum(), searchString, currentOrderFilter));
        }

        // GET: Orders/Details/id
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Order order = orderRepo.GetOrderByID(id);
            if (order == null)
            {
                return HttpNotFound();
            }

            //Do a check to make sure that the order belongs to the Customer logged in or Management
            if (order.customer_number.Equals(GetCustNum()) || User.IsInRole("Management"))
            {
                return View(order);
            }
            else
            {
                return RedirectToAction("Index");
            }
        }

        // GET: Orders/NewOrderView
        public ActionResult NewOrderView()
        {
            return View();
        }

        // POST: Orders/NewOrderView
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult NewOrderView(OrdersViewModel orderVM)
        {
            if (ModelState.IsValid)
            {
                //Create the order object from the placed Order details
                Order o = new Order();
                o.customer_number = GetCustNum();
                o.cost_center = "AJ Omnicare";
                o.order_type = "Pickup";

                o.PickupStop = orderVM.PickupStop;
                o.DeliverStop = orderVM.DeliverStop;

                //Prepare the order object to be sent to the api
                string ApiPayload = PrepareOrderAsStringPayload(o);


                if (SubmitToAPI(ApiPayload))
                {
                    //If it is true, then we know the order was submitted successfully and we can view the details of it
                    return RedirectToAction("Details", new { id = TempData["orderId"] } );
                }
                else
                {   //If the SubmitToAPI Was false, there was a problem in submitting the order.
                    ModelState.AddModelError(string.Empty, "An Error occured while attempting to submit this order. Please try again later.");
                    return View(orderVM);
                }
            }
            //If this is thrown, the model state was not valid, and has errors before submission can be made.
            return View(orderVM);
        }

        public ActionResult Track(int? id)
        {
            if(id != null)
            {
                return View(orderRepo.GetOrderByID(id).DeliverStop);
            }
            else
            {
                return HttpNotFound();
            }
        }


        protected override void Dispose(bool disposing)
        {
            orderRepo.Dispose();
            base.Dispose(disposing);
        }

        private bool SubmitToAPI(string postPayload)
        {
            try
            {
                APIConnection api = new APIConnection(APIConnection.GetAPIKey(), APIConnection.GetCID(), GetCustNum(), "https://api.dwaybill.com/");

                //Get the result from the API Posting
                string[] result = api.PostPayloadAsync(postPayload).Result;
                //Check to see what the result of the API communication was
                if (int.Parse(result[0]) == (int)HttpStatusCode.OK && result[2] != null)
                {
                    //We now need to retrieve the posted order to get all of the new information added to it. 
                    Order order = GetOrderFromAPIPayload(int.Parse(result[2]), GetCustNum());

                    //Check if order is null, if it is, we absolutely don't want that going into the Database 
                    if (order != null)
                    {
                        //Add the order to our local database
                        return InsertOrderIntoDatabase(order);
                    }
                    else
                    {
                        // An Error occured and Order is null, do not insert it into the database
                        return false;                  
                    }
                }
                else if (result[0].Equals("-1"))
                {
                    //There was an API Communication result exception
                    System.Diagnostics.Debug.WriteLine("Error in SUBMIT_TO_API (GenEx): " + result[1] );
                    return false;
                }
                else
                {
                    //The order was not posted successfully in this case, or there was an error
                    return false;
                }
            }
            catch (Exception)
            {
                //If an exception was thrown, handle it here. We need to return false since the SubmitToApi was unsuccessful
                return false;
            }     
        }

        private string GetCustNum()
        {
            var mgr = HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();

            var user = mgr.FindById(User.Identity.GetUserId());

            return user.UserName;
        }

        private bool InsertOrderIntoDatabase(Order order)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    orderRepo.InsertOrder(order);
                    orderRepo.Save();

                    TempData["orderId"] = order.orderId;
                    return true;
                }
            }
            catch (DataException /* dex */)
            {
                //Log the error (uncomment dex variable name after DataException and add a line here to write a log.
                return false;
            }

            return false;
        }

        private string PrepareOrderAsStringPayload(Order order)
        {
           PayloadProcessor p = new PayloadProcessor();
           return p.CreatePayload(order);
        }

        public ActionResult CheckOrderUpdates()
        {
            if (orderRepo.CheckAndUpdateOrders(orderRepo.GetOrders(GetCustNum()), GetCustNum()))
            {
                return RedirectToAction("Index");
            }

            return RedirectToAction("Contact", "Home");
            
        }

        private Order GetOrderFromAPIPayload(int orderNumber, string customerNumber)
        {
            try
            {
                APIConnection Conn = new APIConnection(APIConnection.GetAPIKey(), APIConnection.GetCID(), customerNumber, "https://api.dwaybill.com/");
                string orderPayloadReceived = Conn.GetAPIPayloadAsync(orderNumber).Result;
                //Check to see if there is a null order string
                if (orderPayloadReceived != null)
                {
                    PayloadProcessor p = new PayloadProcessor();
                    return p.ProcessPayloadRecieved(orderPayloadReceived);
                }
                else
                {
                    //Return as there is indeed no order due to an error
                    return null;
                }
            }
            catch(Exception)
            {
                //If there was an Order API Retrieve exception, continue to throw the null back up as there is no order to work with
                return null;
            }            
        }
    }
}
