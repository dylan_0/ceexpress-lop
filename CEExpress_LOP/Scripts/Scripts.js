﻿
/*----------------------------------------*/
/*JS for _NavigationBar*/
/*----------------------------------------*/

///---------------///---------------///---------------///---------------
//rezising navbar action
(new IntersectionObserver(function (e, o) {
    if (e[0].intersectionRatio > 0) {
        document.documentElement.removeAttribute('class');
    } else {
        document.documentElement.setAttribute('class', 'stuck');
    };
})).observe(document.querySelector('.trigger'));


//scroll indicator action
window.onscroll = function () { myFunction() };
function myFunction() {
    var winScroll = document.body.scrollTop || document.documentElement.scrollTop;
    var height = document.documentElement.scrollHeight - document.documentElement.clientHeight;
    var scrolled = (winScroll / height) * 100;
    document.getElementById("myBar").style.width = scrolled + "%";
}



/*----------------------------------------*/
/*JS for NewOrderView*/
/*----------------------------------------*/
///---------------///---------------///---------------///---------------

$(document).ready(function () {

    //alert("Its me fool!");
    $(".cartStep1").click(function () {
        $("#first").show();
        $("#second").hide();
        $("#third").hide();
        $("#fourth").hide();

    });
    $(".cartStep2").click(function () {
        $("#first").hide();
        $("#second").show();
        $("#third").hide();
        $("#fourth").hide();
    });
    $(".cartStep3").click(function () {
        $("#first").hide();
        $("#second").hide();
        $("#third").show();
        $("#fourth").hide();

    });
    //$(".cartStep4").click(function () {
    //    //$("#first").hide();
    //    //$("#second").hide();
    //    //$("#third").hide();
    //    //$("#fourth").show();
    //});

    $("#next_1").click(function () {
        $("#second").show();
        $("#first").hide();
        //for progress bar
        $("#progressBar").css("width", "50%");
        $("#progressText").html("Step - 2");
    });

    $("#previous_2").click(function () {
        $("#second").hide();
        $("#first").show();
        //for progress bar
        $("#progressBar").css("width", "25%");
        $("#progressText").html("Step - 1");
    });

    $("#next_2").click(function () {
        $("#third").show();
        $("#second").hide();
        //for progress bar
        $("#progressBar").css("width", "75%");
        $("#progressText").html("Step - 3");
    });

    $("#previous_3").click(function () {
        $("#third").hide();
        $("#second").show();
        //for progress bar
        $("#progressBar").css("width", "50%");
        $("#progressText").html("Step - 2");
    });

    $("#next_3").click(function () {
        $("#fourth").show();
       //confimation info here
        confirmationValues();


        $("#third").hide();
        //for progress bar
        $("#progressBar").css("width", "100%");
        $("#progressText").html("Step - 4");
    });

    $("#previous_4").click(function () {
        $("#third").show();
        $("#fourth").hide();
        //for progress bar
        $("#progressBar").css("width", "75%");
        $("#progressText").html("Step - 3");
    });


    $(window).on('resize', function () { onResize }).trigger('resize');

});

confirmationValues = function () {

    //pickup info
    $('#p_companyConf').html($('#PickupStop_company').val());

    $('#p_nameConf').html($('#PickupStop_name').val());

    $('#p_phoneConf').html($('#PickupStop_phone').val());

    $('#p_addressConf').html($('#PickupStop_address').val());

    $('#p_suiteConf').html($('#PickupStop_suite').val());

    $('#p_countryConf').html($('#PickupStop_country').val());

    $('#p_stateConf').html($('#PickupStop_state').val());

    $('#p_cityConf').html($('#PickupStop_city').val());

    $('#p_postal_codeConf').html($('#PickupStop_postal_code').val());


    //package info
    $('#special_instructionsConf').html($('#DeliverStop_special_instructions').val());

    $('#service_typeConf').html($('#DeliverStop_service_type').val());

    $('#packageConf').html($('#DeliverStop_package').val());

    $('#number_of_piecesConf').html($('#DeliverStop_number_of_pieces').val());

    $('#weightConf').html($('#DeliverStop_weight').val());

    //delivery info

    $('#d_companyConf').html($('#DeliverStop_company').val());

    $('#d_nameConf').html($('#DeliverStop_name').val());

    $('#d_phoneConf').html($('#DeliverStop_phone').val());

    $('#d_addressConf').html($('#DeliverStop_address').val());

    $('#d_suiteConf').html($('#DeliverStop_suite').val());

    $('#d_countryConf').html($('#DeliverStop_country').val());

    $('#d_stateConf').html($('#DeliverStop_state').val());

    $('#d_cityConf').html($('#DeliverStop_city').val());

    $('#d_postal_codeConf').html($('#DeliverStop_postal_code').val());


}

// page resizing
onResize = function () {
    $('#progressBarBack').hide();
    $('#progressBar').hide();
    $('.cartBar').hide();
    $(".progress").css("background-color", "rgb(248, 249, 250)");

    $(window).resize(function () {

        if ($(this).width() < 600) {
            $('#progressBarBack').show();
            $('#progressBar').show();
            $('.cartBar').show();
            $('.cart-steps').hide();
            $(".progress").css("background-color", "#e9ecef");
        }
        else {
            $('#progressBarBack').hide();
            $('#progressBar').hide();
            $('.cartBar').hide();
            $('.cart-steps').show();
        }
    });
}


$(window).bind('resize', onResize);

///adding values to confimation page
//$(document).ready(function () {
//    var x = $('#DeliverStop_special_instructions').val();
//    $('#special_instructionsConf').html(x);
//});




///---------------///---------------///---------------///---------------

//; (function ($) {

//    function validateSelect(select) {
//        var selectWrapper = select.closest('.selectric-wrapper');

//        if ((select.val() == null || select.val() == '') && select.is('[required]')) {
//            selectWrapper.removeClass('selectric-valid');
//            selectWrapper.addClass('selectric-invalid');
//        } else {
//            selectWrapper.removeClass('selectric-invalid');
//            selectWrapper.addClass('selectric-valid');
//        }
//    }

//    // Custom Select
//    var customSelect = $('.custom-select');

//    customSelect.selectric({
//        responsive: true
//    });

//    // Tooltip

//    $('[data-toggle="tooltip"]').tooltip();

//    // Upload File

//    $('.custom-file-input').on('change', function () {
//        var file = $(this),
//            value = file.val().split('\\').pop();

//        if (value) {
//            file.closest('.custom-file').find('.form-control').html(value);
//        }
//    });

//    // Validate Form

//    $('form').on('submit', function () {
//        customSelect.each(function () {
//            validateSelect($(this));
//        });
//    });

//    window.addEventListener('load', function () {
//        var forms = document.getElementsByClassName('needs-validation');

//        var validation = Array.prototype.filter.call(forms, function (form) {
//            form.addEventListener('submit', function (event) {
//                if (form.checkValidity() === false) {
//                    event.preventDefault();
//                    event.stopPropagation();
//                }

//                form.classList.add('was-validated');

//                $('.custom-select').on('selectric-change', function (event, element, selectric) {
//                    validateSelect($(element));
//                });
//            }, false);
//        });
//    }, false);

//    // Toggle Checkbox

//    $('[data-toggle]').on('change', function () {
//        var toggleItem = $(this).data('toggle');

//        if (this.checked) {
//            $('#' + toggleItem).addClass('d-none');
//        } else {
//            $('#' + toggleItem).removeClass('d-none');
//        }
//    });

//})(jQuery);
/////---------------///---------------///---------------///---------------


/*----------------------------------------*/
/*JS for ~/Management/Home*/
/*----------------------------------------*/
///---------------///---------------///---------------///---------------

//$(document).ready(function () {
//    $('.element-1, .element-2, .element-3, .element-4').css('display', 'none')
//});

//$(document).ready(function () {
//    $('.homeText').css("background-color", "white")
//});

$('.a-1').hover(
    function () {
        $('.element-1').show()
        $('#a1').addClass('active');
        $('.element-2, .element-3, .element-4').hide()
        //$('.element-1').css('display', 'block')
        $('#a2, #a3, #a4').removeClass('active');
        $("#a1").css("background-color", " rgb(246, 144, 61)");
        $("#a1").css("color", " white");
        $(".homeText").css("background-color", "rgba(168, 170, 234, 0.25)");
    }
    ,
    function () {
        $("#a1").css("background-color", "rgba(249, 173, 46, 0.75)");
        $("#a1").css("color", " rgba(39, 35, 29, 0.75)");
        //$('.element-1').css('display', 'none')
        //$('.a-2, .a-3, .a-4').find('.active').removeClass('active');
    }
);
$('.a-2').hover(
    function () {
        $('.element-2').show()
        $('#a2').addClass('active');
        $('.element-1, .element-3, .element-4').hide()
        //$('.element-2').css('display', 'block')
        $('#a1, #a3, #a4').removeClass('active');
        $("#a2").css("background-color", " rgb(246, 144, 61)");
        $("#a2").css("color", " white");
        $(".homeText").css("background-color", "rgba(168, 170, 234, 0.25)");
    },
    function () {
        $("#a2").css("background-color", "rgba(249, 173, 46, 0.75)");
        $("#a2").css("color", " rgba(39, 35, 29, 0.75)");
        //$('.element-2').css('display', 'none')
        //$('.a-1, .a-2, .a-4').find('.active').removeClass('active');
    });

$('.a-3').hover(
    function () {
        $('.element-3').show()
        $('#a3').addClass('active');
        $('.element-1, .element-2, .element-4').hide();
        //$('.element-3').css('display', 'block')
        $('#a1, #a2, #a4').removeClass('active');
        $("#a3").css("background-color", " rgb(246, 144, 61)");
        $("#a3").css("color", " white");
        $(".homeText").css("background-color", "rgba(168, 170, 234, 0.25)");
    },
    function () {
        $("#a3").css("background-color", "rgba(249, 173, 46, 0.75)");
        $("#a3").css("color", " rgba(39, 35, 29, 0.75)");
        //$('.element-3').css('display', 'none')
        //$('.a-1, .a-2, .a-4').find('.active').removeClass('active');
    });


$('.a-4').hover(
    function () {
        $('.element-4').show();
        $('#a4').addClass('active');
        $('.element-1, .element-2, .element-3').hide();
        //$('.element-4').css('display', 'block')
        $('#a1, #a2, #a3').removeClass('active');
        $("#a4").css("background-color", " rgb(246, 144, 61)");
        $("#a4").css("color", " white");
        $(".homeText").css("background-color", "rgba(168, 170, 234, 0.25)");
    },
    function () {
        $("#a4").css("background-color", "rgba(249, 173, 46, 0.75)");
        $("#a4").css("color", " rgba(39, 35, 29, 0.75)");
        //$('.element-4').css('display', 'none')
        //$('.a-1, .a-2, .a-4').find('.active').removeClass('active');
    });

///---------------///---------------///---------------///---------------



/*----------------------------------------*/
/*JS for /
/*----------------------------------------*/
///---------------///---------------///---------------///---------------



///---------------///---------------///---------------///---------------
