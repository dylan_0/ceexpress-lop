﻿using LOP_DatabaseModel.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CEExpress_LOP.Core.Repository
{
   public interface IOrderRepository : IDisposable
    {
        IEnumerable<Order> GetOrders(string v);
        IEnumerable<Order> GetOrders();
        IEnumerable<Order> GetCustOrdersWithSortRequest(string sortRequest, string cutNumber, string searchString, string currentOrderFilter);
        Order GetOrderByID(int? OrderId);
        void UpdateOrder(Order order);
        bool CheckAndUpdateOrders(IEnumerable<Order> ordersCollection, string custNumber);

        void InsertOrder(Order order);
        void Save();

    }
}
