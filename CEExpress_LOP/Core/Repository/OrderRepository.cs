﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using LOP_DatabaseModel.Model;

namespace CEExpress_LOP.Core.Repository
{
    public class OrderRepository : IOrderRepository, IDisposable
    {

        private CEExpressDBEntities dbCtx;
        private bool ctxDisposed = false;

        public OrderRepository(CEExpressDBEntities dbCtx)
        {
            this.dbCtx = dbCtx;
        }

        public void Dispose()
        {
            dbCtx.Dispose();
            GC.SuppressFinalize(this);
        }
        protected virtual void Dispose(bool disposing)
        {
            if (!this.ctxDisposed)
            {
                if (disposing)
                {
                   dbCtx.Dispose();
                }
            }
            this.ctxDisposed = true;
        }

        public Order GetOrderByID(int? OrderId)
        {
            return dbCtx.Orders.Find(OrderId);
        }

        public IEnumerable<Order> GetOrders()
        {
            return dbCtx.Orders.ToList();
        }

        public IEnumerable<Order> GetCustOrdersWithSortRequest(string sortRequest, string custNum, string searchString, string currentOrderFilter)
        {
            IEnumerable<Order> ord = null;

            if (custNum.Equals("Management"))
            {
                if (!String.IsNullOrEmpty(searchString))
                {
                    ord = dbCtx.Orders.Where(o => o.customer_number.Contains(searchString) || o.cost_center.Contains(searchString) || o.status.Contains(searchString) || o.status_date.Contains(searchString));
                }
                else {
                    ord = GetOrders().Where(c => !c.status.Equals(DW_Status.Cancelled.ToString()) && !c.status.Equals(DW_Status.Delivered.ToString()));
                }
            }
            else
            {
                if (!String.IsNullOrEmpty(searchString))
                {
                    ord = dbCtx.Orders.Where(o => (o.customer_number.Equals(custNum) ) && ( o.cost_center.Contains(searchString) || o.order_number.ToString().Contains(searchString) || o.status.Contains(searchString) || o.status_date.Contains(searchString) ));
                }
                else
                {
                    ord = dbCtx.Orders.Where(c => c.customer_number.Equals(custNum));
                }
            }

           


            switch (sortRequest)
            {
                case "orderNum_desc":
                    ord = ord.OrderByDescending(o => o.order_number);
                    break;
                case "custNumber_desc":
                    ord = ord.OrderByDescending(o => o.customer_number);
                    break;
                case "Customer Number":
                    ord = ord.OrderBy(o => o.customer_number);
                    break;
                case "costCenter_desc":
                    ord = ord.OrderByDescending(o => o.cost_center);
                    break;
                case "Cost Centre":
                    ord = ord.OrderBy(o => o.cost_center);
                    break;
                case "statusDate_desc":
                    ord = ord.OrderByDescending(o => o.status_date);
                    break;
                case "Status Date":
                    ord = ord.OrderBy(o => o.status_date);
                    break;
                case "status_desc":
                    ord = ord.OrderByDescending(o => o.status);
                    break;
                case "Status":
                    ord = ord.OrderBy(o => o.status);
                    break;
                default:
                    ord = ord.OrderBy(o => o.order_number);
                    break;
            }

            return ord;
        }


        public void InsertOrder(Order order)
        {
            dbCtx.Orders.Add(order);
        }

        public void Save()
        {
            dbCtx.SaveChanges();
        }

        public void UpdateOrder(Order order)
        {
            dbCtx.Entry(order).State = EntityState.Modified;
            
        }

        public IEnumerable<Order> GetOrders(string cust_num)
        {
            return dbCtx.Orders.Where(c => c.customer_number.Equals(cust_num));
        }

        public bool CheckAndUpdateOrders(IEnumerable<Order> ordersCollection, string custNumber)
        {
            try
            {
                //We need to check the API and fetch for orders which have a new status date which is not the same as the local copy. Then we can update those orders in our local database
                //Access the APIConnection
                APIConnection api = new APIConnection(APIConnection.GetAPIKey(), APIConnection.GetCID(), custNumber, "https://api.dwaybill.com/");

                //This string will contain the large JSON payload of all orders for the customer
                string ordersPayload = api.GetAPIOrdersPayloadAsync().Result;

                //If the ordersPayload is null, then there was an exception error, we don't want to continue
                if(ordersPayload == null)
                {
                    return false;
                }

                //Make a new Payload Processor to handle all the orders
                PayloadProcessor p = new PayloadProcessor();
                IEnumerable<Order> APIorders = p.ProcessOrdersPayloadRecieved(ordersPayload);

                //We need to check to see if the orders list is null
                if(APIorders == null)
                {
                    return false;
                }

                //Now we need to grab all of the orders from the local DB
                IEnumerable<Order> localOrders = dbCtx.Orders.ToList().Where(o => o.customer_number.Equals(custNumber));

                //Here is a list of local orders to update
                List < Order > localOrdersToUpdate = new List<Order>();


                //Loop through each local Order and apply changes where there is a mis-matched status
                foreach (var localOrder in localOrders)
                {
                    Order ao = APIorders.Where(a => a.order_number == localOrder.order_number).First();
                   // DeliverStop deliverStop = ao.DeliverStop;
                   // PickupStop pickupStop = ao.PickupStop;
                    if (!ao.version.Equals(localOrder.version))
                    {
                        //Check to see if the local order's status date is the same as the API order
                        localOrder.deliver_by = ao.deliver_by;
                        localOrder.dispatch_driver = ao.dispatch_driver;
                        localOrder.final_price = ao.final_price;
                        localOrder.price = ao.price;
                        localOrder.read = ao.read;
                        localOrder.ready_time = ao.ready_time;
                        localOrder.status = ao.status;
                        localOrder.status_date = ao.status_date;
                      //  localOrder.DeliverStop = deliverStop;
                       // localOrder.PickupStop = pickupStop;
                    }
                }
                //Save the changes to the database
                dbCtx.SaveChanges();

                return true;

            }
            catch(Exception ex)
            {
                System.Diagnostics.Debug.WriteLine("! *** -> Error in CHECK_AND_UPDATE_ORDERS: " + ex.Message);

                return false;
            }
            
        }
    }
}