﻿namespace CEExpress_LOP.Core
{
    //
    public enum OrderType
    {
        PICKUP, //The order's cost center is attached to the pickup stop.
        DELIVERY, //The order's cost center is attached to the delivery stop.
        THIRD_PARTY, //The order's cost center is not attached to any of the stops.
    };
}