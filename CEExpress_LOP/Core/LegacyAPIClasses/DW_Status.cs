﻿namespace CEExpress_LOP.Core
{
    public enum DW_Status
    {
        New = 1, //The order is a new order.
        Dispatched = 2, //The order has been dispatched to a driver.
        PickedUp = 3, //The order has been picked up by the driver.
        Completed = 4, //The order has been completed.
        Cancelled = 5, //The order has been cancelled.
        Delivered = 6, //The order has been delivered by the driver.
        Confirmed = 7, //The order has been acknowledged and accepted by the driver.
        Undefined = 8, //The order is not in any of the defined states.
    }
}