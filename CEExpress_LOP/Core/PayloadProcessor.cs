﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.Script.Serialization;
using LOP_DatabaseModel.Model;

namespace CEExpress_LOP.Core
{
    public class PayloadProcessor
    {
        public string CreatePayload(Order newOrder)
        {
            try
            {
                JavaScriptSerializer js = new JavaScriptSerializer();

                //Seralize the Three Components of the Order: Order, PickupStop, DeliverStop
                string Order_Str = js.Serialize(newOrder);
                string Pickup_Str = js.Serialize(newOrder.PickupStop);
                string Deliver_Str = js.Serialize(newOrder.DeliverStop);

                //Append The Pickup and Deliver Stops to the Order_Str JSON As Route_Stop array items
                JObject Order_jobj = JObject.Parse(Order_Str);
                Order_jobj.Add(new JProperty("route_stops"));

                JArray route_stops = (JArray)Order_jobj["route_stops"];
                JObject PickupObj = JObject.Parse(Pickup_Str);
                JObject DeliverObj = JObject.Parse(Deliver_Str);

                //New Object Based on contact information for the Pickup and Deliver Stop
                JObject pickup_Contact = new JObject(new JProperty("name", PickupObj["name"]), new JProperty("phone", PickupObj["phone"]));
                JObject deliver_Contact = new JObject(new JProperty("name", DeliverObj["name"]), new JProperty("phone", DeliverObj["phone"]));

                //Add new properties to each pickup and deliver stop
                PickupObj.Add(new JProperty("contact", pickup_Contact));
                DeliverObj.Property("country").AddAfterSelf(new JProperty("contact", deliver_Contact));

                //Add the contacts object into the respective structure
            //    PickupObj.Add(pickup_Contact);
            //    DeliverObj.Property("country").AddAfterSelf(deliver_Contact);

                //Create a new array in the JSON String for route_stops
                JArray Route_Stops = (JArray)Order_jobj["route_stops"];

                //Extract the contact information from the route-stop property
                

                //Add the Pickup and Deliver stop values to the route stops array
                Route_Stops.Add(PickupObj);
                Route_Stops.Add(DeliverObj);

                //Convert the Compiled Order to a JSON payload string 
                string payload = Order_jobj.ToString(Newtonsoft.Json.Formatting.None);

                //Return the serialized compiled order as a string containing the strutured JSON 
                return payload;

            }
            catch(Exception)
            {
                //If there was an error in creating a payload - return null and let the calling method handle
                return null;
            }
            
        }

        public Order ProcessPayloadRecieved(string responsePayload)
        {
            try
            {
                JObject jObj = JObject.Parse(responsePayload);

                //Grab the body of the JSON Response
                JObject body = (JObject)jObj["body"];

                return DeserializeOrder(body);  
            }
            catch(Exception)
            {
                //Return a null if there was an exception in processing a recieved payload, as there is no guarentee of Order structure validity
                return null;
            }
        }

        private Order DeserializeOrder(JObject body)
        {
            try
            {
              
                //Separate the Route Stops array from the body
                JArray routeStop = (JArray)body["route_stops"];
                //Select the Pickup and Deliver Stop data
                JObject pickupstop = (JObject)routeStop[0];
                JObject deliverstop = (JObject)routeStop[1];

                //Grab the contacts
                JObject pickup_Contact = (JObject)pickupstop["contact"];
                JObject deliver_Contact = (JObject)deliverstop["contact"];

                //Assign the values of the contacts
                pickupstop["name"] = (string)pickup_Contact["name"];
                pickupstop["phone"] = (string)pickup_Contact["phone"];
                deliverstop["name"] = (string)deliver_Contact["name"];
                deliverstop["phone"] = (string)deliver_Contact["phone"];

                //TODO Check to see if there is more than just one deliver stop

                string Body = body.ToString(Newtonsoft.Json.Formatting.None);
                string Pickup = pickupstop.ToString(Newtonsoft.Json.Formatting.None);
                string Deliver = deliverstop.ToString(Newtonsoft.Json.Formatting.None);

                //Create Instances and deseralize json
                JavaScriptSerializer js = new JavaScriptSerializer();
                Order order = js.Deserialize<Order>(Body);
                PickupStop pickup = js.Deserialize<PickupStop>(Pickup);
                DeliverStop deliver = js.Deserialize<DeliverStop>(Deliver);

                //Append the Pickup and Deliver objects into the main order
                order.PickupStop = pickup;
                order.DeliverStop = deliver;

                //Return the DigitalWaybill Order details after processing
                return order;

            }
            catch (Exception)
            {
                //TODO Output exception
                return null;
            }
        }

        internal IEnumerable<Order> ProcessOrdersPayloadRecieved(string ordersPayload)
        {
            List<Order> ordersCollection;
            IEnumerable<Order> orderList;
            //Check to see how many orders need to be processed
            try
            {
                JObject jObj = JObject.Parse(ordersPayload);


                //Initalie the Order collection
                ordersCollection = new List<Order>();
                //Ensure that there were orders returned
                int pageSize = (int)jObj["body"]["page_size"];
                int count = (int)jObj["body"]["count"];

                if (count >= 1)
                {
                  
                    //Grab the Orders array of the JSON Response
                    JArray orders = (JArray)jObj["body"]["orders"];

                    //Loop through all the orders in the array and deseralize them into orders
                    for(int i = 0; i < pageSize; i++)
                    {
                        JObject currentOrder = (JObject)orders[i];
                        Order o = DeserializeOrder(currentOrder);

                        if(o != null)
                        {
                            ordersCollection.Add(o);
                        }
                    }


                    //Return the collection as an IEnumerable 
                   return orderList = ordersCollection;
                }
                else
                {
                    return orderList = null;
                }

            }
            catch (Exception)
            {
                //Return a null if there was an exception in processing a recieved payload, as there is no guarentee of Order structure validity
                return null;
            }
        }
    }
}