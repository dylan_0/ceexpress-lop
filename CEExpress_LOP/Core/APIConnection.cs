﻿using Newtonsoft.Json.Linq;
using System;
using System.Configuration;
using System.Net.Http;
using System.Net.Http.Headers;

namespace CEExpress_LOP.Core
{
    internal class APIConnection
    {
        private readonly string API_KEY;
        private readonly int CID;
        private readonly string CUSTOMER_NUMBER;
        private readonly string API_URL;

        private readonly HttpClient client; 

        public APIConnection(string apiKey, int cid, string customerNumber, string apiURL)
        {
            //Validate constructor parameters
            if(apiKey.Equals("-1") || cid == -1 || customerNumber == null || apiURL == null)
            {
                throw new Exception("Invalid parameters recieved - Unable to create new APIConnection. ");
            }

            //Set the required values for API Connection 
            API_KEY = apiKey;
            CID = cid;
            CUSTOMER_NUMBER = customerNumber;
            API_URL = apiURL;

            //Initalize the HTTPClient used to handle the requests
            client = new HttpClient();

            //Set the base address for where the connection should be going to, in our case, the API Url along with the Courier id
            string baseAddress = string.Concat(API_URL, CID);
            client.BaseAddress = new Uri(baseAddress + "/");
            client.DefaultRequestHeaders.Accept.Clear();

            //Specify that we want to work with the JSON payload from the DigitalWaybill API
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

        }

        internal async System.Threading.Tasks.Task<string[]> PostPayloadAsync(string payload)
        {
            //Convert the string payload into a StringContent that the client will send to the API
            var content = new StringContent(payload, System.Text.Encoding.UTF8, "application/json");

            try
            {
                //Send the content to the API
                HttpResponseMessage response = client.PostAsync("orders.json?v=1&key=" + API_KEY + "&customer_number="+ CUSTOMER_NUMBER + "&password=pass", content).Result;

                //If we recieved a successful result from the API
                response.EnsureSuccessStatusCode();

               //Return the the newly created order number if successful status code
               return await APICommunicationResultAsync(response);
           
              
            }catch(HttpRequestException ex)
            {
                //If there was an exception during the POST to Api, handle it here and return -1 in the payload comm sequence. 
                string[] ExReturn = new string[] { "-1", ex.Message, "NULL" };
                System.Diagnostics.Debug.WriteLine("Error in POST_PAYLOD_ASYNC (HttpRequestException): " + ex.Message);
                return ExReturn;
            }
        }

        public async System.Threading.Tasks.Task<string> GetAPIOrdersPayloadAsync()
        {
            string ResponsePayload = null;
            try
            {
                //This will request a JSON response from the API - All orders for a customer
                HttpResponseMessage response = client.GetAsync("orders.json?v=1&key=" + API_KEY + "&customer_number=" + CUSTOMER_NUMBER + "&password=pass").Result;

                //If the response from the API was successful, we need to process that response payload into a string and return it
                response.EnsureSuccessStatusCode();

                ResponsePayload = await response.Content.ReadAsStringAsync();
                return ResponsePayload;
            }
            catch (HttpRequestException ex)
            {
                //If the response returned an unsucessful status code, then we will return a null value, which will be handeled in the calling method.
                System.Diagnostics.Debug.WriteLine("Error in GET_API_ORDERS_PAYLOAD_ASYNC: " + ex.Message);
                return ResponsePayload;
            }


        }

        private async System.Threading.Tasks.Task<string[]> APICommunicationResultAsync(HttpResponseMessage response)
        {
            try
            {
                //Process the responseJSON and break it down for further interpretation
                string responseJson = await response.Content.ReadAsStringAsync();
                JObject responseObject = JObject.Parse(responseJson);

                //Status Code
                string StatusCode = (string)responseObject["status"];
                //Error Message
                string ErrorMessage = (string)responseObject["error"];

                //Order Number - Currently we do not know if there was one provided - Assuming not until verified in the next step
                string OrderNumber = null;

                //Body & Order Number - Check to see if there is an order number as found in the body property
                if (responseObject["body"].HasValues)
                {
                    JObject BodyOrderNumber = (JObject)responseObject["body"];
                    OrderNumber = (string)BodyOrderNumber["order_number"];
                }

                //Combine the three parts of the Response from the API into an array for further processing in the calling methods
                string[] CommResults = new string[] { StatusCode, ErrorMessage, OrderNumber };

                return CommResults;
            }
            catch (Exception ex)
            {
                //If there was an exception during the POST to Api, handle it here and return -1 in the payload comm sequence. 
                string[] ExReturn = new string[] { "-1", ex.Message, "NULL" };
                System.Diagnostics.Debug.WriteLine("Error in API_COMMUNICATION_RESULT_ASYNC: " + ex.Message);
                return ExReturn;
            }   
        }

        public async System.Threading.Tasks.Task<string> GetAPIPayloadAsync(int OrderNumber)
        {
            string ResponsePayload = null;
            try {
                //This will request a JSON response from the API based on a requested OrderNumber
                HttpResponseMessage response = client.GetAsync("orders.json/" + OrderNumber + "?v=1&key=" + API_KEY + "&customer_number=" + CUSTOMER_NUMBER + "&password=pass").Result;

                //If the response from the API was successful, we need to process that response payload into a string and return it
                response.EnsureSuccessStatusCode();

                ResponsePayload = await response.Content.ReadAsStringAsync();
                    return ResponsePayload;
            
                
            }
            catch (HttpRequestException ex)
            {
                //If the response returned an unsucessful status code, then we will return a null value, which will be handeled in the calling method.
                System.Diagnostics.Debug.WriteLine("Error in GET_API_PAYLOAD_ASYNC: " + ex.Message);
                return ResponsePayload;
            }
            

          
        }

        public static string GetAPIKey()
        {
            try {
                string key = RetrieveConfigValue("apikey");
                //Return the INT ApiKey
                return key;
            }
            catch(Exception)
            {
                //If there was an exception in checking for the apikey, it will be caught here
                return "-1";
            }
  
        }

        public static int GetCID()
        {
            try
            {
                int cid = int.Parse(RetrieveConfigValue("cid"));

                //Return the INT Cid
                return cid;
            }
            catch (Exception)
            {
                
                //If there was an exception in checking for the cid, it will be caught here
                return -1;
            }

        }

        private static string RetrieveConfigValue(string key)
        {
            //Grab the value of the key from the Web.config using the AppSettings of ConfigManager
            string keyValue = ConfigurationManager.AppSettings.Get(key);

            //Check to see if there is an associated value of the requested key
            if(keyValue != null)
            {
                return keyValue;
            }
            else
            {
                throw new Exception("Key value not found.");
            }
        }

    }
}