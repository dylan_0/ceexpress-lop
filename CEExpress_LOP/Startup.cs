﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(CEExpress_LOP.Startup))]
namespace CEExpress_LOP
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
