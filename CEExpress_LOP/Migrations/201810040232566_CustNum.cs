namespace CEExpress_LOP.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class CustNum : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.AspNetUsers", "customer_number", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.AspNetUsers", "customer_number");
        }
    }
}
