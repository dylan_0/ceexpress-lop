namespace CEExpress_LOP.Migrations
{
    using CEExpress_LOP.Models;
    using Microsoft.AspNet.Identity;
    using Microsoft.AspNet.Identity.EntityFramework;
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<CEExpress_LOP.Models.ApplicationDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = true;
        }

        protected override void Seed(CEExpress_LOP.Models.ApplicationDbContext context)
        {
            var usrStore = new UserStore<ApplicationUser>(context);
            var usrManager = new UserManager<ApplicationUser>(usrStore);

            if (!context.Users.Any(t => t.Email == "mgmt@ceexpress.com"))
            {
                var user = new ApplicationUser { Email = "Management", UserName = "Management", customer_number = "Management" };
                usrManager.Create(user, "admin01*");

                context.Roles.AddOrUpdate(r=>r.Name ,new IdentityRole { Name = "Management" });
                context.SaveChanges();

                usrManager.AddToRole(user.Id, "Management");
            }

            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data.
        }
    }
}
