
-- --------------------------------------------------
-- Entity Designer DDL Script for SQL Server 2005, 2008, 2012 and Azure
-- --------------------------------------------------
-- Date Created: 11/27/2018 17:55:26
-- Generated from EDMX file: C:\Users\Developer\source\repos\ceexpress-lop\LOP_DatabaseModel\Model\CEExpressDBModel.edmx
-- --------------------------------------------------

SET QUOTED_IDENTIFIER OFF;
GO
USE [LOP_Database];
GO
IF SCHEMA_ID(N'dbo') IS NULL EXECUTE(N'CREATE SCHEMA [dbo]');
GO

-- --------------------------------------------------
-- Dropping existing FOREIGN KEY constraints
-- --------------------------------------------------

IF OBJECT_ID(N'[dbo].[FK_OrderDeliverStop]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Orders] DROP CONSTRAINT [FK_OrderDeliverStop];
GO
IF OBJECT_ID(N'[dbo].[FK_OrderPickupStop]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Orders] DROP CONSTRAINT [FK_OrderPickupStop];
GO

-- --------------------------------------------------
-- Dropping existing tables
-- --------------------------------------------------

IF OBJECT_ID(N'[dbo].[DeliverStops]', 'U') IS NOT NULL
    DROP TABLE [dbo].[DeliverStops];
GO
IF OBJECT_ID(N'[dbo].[Orders]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Orders];
GO
IF OBJECT_ID(N'[dbo].[PickupStops]', 'U') IS NOT NULL
    DROP TABLE [dbo].[PickupStops];
GO

-- --------------------------------------------------
-- Creating all tables
-- --------------------------------------------------

-- Creating table 'Orders'
CREATE TABLE [dbo].[Orders] (
    [orderId] int IDENTITY(1,1) NOT NULL,
    [id] int  NULL,
    [time] nvarchar(max)  NULL,
    [order_number] int  NULL,
    [price] nvarchar(max)  NULL,
    [customer_number] nvarchar(max)  NOT NULL,
    [cost_center] nvarchar(max)  NOT NULL,
    [final_price] float  NULL,
    [flagged] int  NULL,
    [read] bit  NULL,
    [pending] bit  NULL,
    [origin] nvarchar(max)  NULL,
    [status_date] nvarchar(max)  NULL,
    [ready_time] nvarchar(max)  NULL,
    [deliver_by] nvarchar(max)  NULL,
    [dispatch_driver] nvarchar(max)  NULL,
    [version] nvarchar(max)  NULL,
    [status] nvarchar(max)  NULL,
    [order_type] nvarchar(max)  NULL,
    [PickupStop_PStopId] int  NOT NULL,
    [DeliverStop_DStopId] int  NOT NULL
);
GO

-- Creating table 'PickupStops'
CREATE TABLE [dbo].[PickupStops] (
    [PStopId] int IDENTITY(1,1) NOT NULL,
    [company] nvarchar(max)  NULL,
    [address] nvarchar(max)  NULL,
    [suite] nvarchar(max)  NULL,
    [city] nvarchar(max)  NULL,
    [state] nvarchar(max)  NULL,
    [postal_code] nvarchar(max)  NULL,
    [country] nvarchar(max)  NULL,
    [name] nvarchar(max)  NULL,
    [phone] nvarchar(max)  NULL
);
GO

-- Creating table 'DeliverStops'
CREATE TABLE [dbo].[DeliverStops] (
    [DStopId] int IDENTITY(1,1) NOT NULL,
    [company] nvarchar(max)  NULL,
    [address] nvarchar(max)  NULL,
    [suite] nvarchar(max)  NULL,
    [city] nvarchar(max)  NULL,
    [state] nvarchar(max)  NULL,
    [postal_code] nvarchar(max)  NULL,
    [country] nvarchar(max)  NULL,
    [name] nvarchar(max)  NULL,
    [phone] nvarchar(max)  NULL,
    [paper_waybill] nvarchar(max)  NULL,
    [special_instructions] nvarchar(max)  NULL,
    [return_add] nvarchar(max)  NULL,
    [service_type] nvarchar(max)  NULL,
    [package] nvarchar(max)  NULL,
    [number_of_pieces] int  NULL,
    [weight] int  NULL,
    [vehicle] nvarchar(max)  NULL,
    [driver_number] nvarchar(max)  NULL,
    [dispatch_message] nvarchar(max)  NULL,
    [notes] nvarchar(max)  NULL,
    [signature_contact] nvarchar(max)  NULL,
    [reference] nvarchar(max)  NULL,
    [signature] nvarchar(max)  NULL,
    [signature_lines] nvarchar(max)  NULL,
    [fuel_surcharge] nvarchar(max)  NULL,
    [route_stop_id] int  NULL,
    [route_status] nvarchar(max)  NULL,
    [distance] int  NULL,
    [air_distance] int  NULL,
    [route_status_detail] nvarchar(max)  NULL,
    [driver_pricelist] nvarchar(max)  NULL,
    [route_status_date] nvarchar(max)  NULL,
    [receive_date] nvarchar(max)  NULL,
    [dispatch_date] nvarchar(max)  NULL,
    [pickup_date] nvarchar(max)  NULL,
    [delivery_date] nvarchar(max)  NULL,
    [cancel_date] nvarchar(max)  NULL,
    [confirm_date] nvarchar(max)  NULL
);
GO

-- --------------------------------------------------
-- Creating all PRIMARY KEY constraints
-- --------------------------------------------------

-- Creating primary key on [orderId] in table 'Orders'
ALTER TABLE [dbo].[Orders]
ADD CONSTRAINT [PK_Orders]
    PRIMARY KEY CLUSTERED ([orderId] ASC);
GO

-- Creating primary key on [PStopId] in table 'PickupStops'
ALTER TABLE [dbo].[PickupStops]
ADD CONSTRAINT [PK_PickupStops]
    PRIMARY KEY CLUSTERED ([PStopId] ASC);
GO

-- Creating primary key on [DStopId] in table 'DeliverStops'
ALTER TABLE [dbo].[DeliverStops]
ADD CONSTRAINT [PK_DeliverStops]
    PRIMARY KEY CLUSTERED ([DStopId] ASC);
GO

-- --------------------------------------------------
-- Creating all FOREIGN KEY constraints
-- --------------------------------------------------

-- Creating foreign key on [PickupStop_PStopId] in table 'Orders'
ALTER TABLE [dbo].[Orders]
ADD CONSTRAINT [FK_OrderPickupStop]
    FOREIGN KEY ([PickupStop_PStopId])
    REFERENCES [dbo].[PickupStops]
        ([PStopId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_OrderPickupStop'
CREATE INDEX [IX_FK_OrderPickupStop]
ON [dbo].[Orders]
    ([PickupStop_PStopId]);
GO

-- Creating foreign key on [DeliverStop_DStopId] in table 'Orders'
ALTER TABLE [dbo].[Orders]
ADD CONSTRAINT [FK_OrderDeliverStop]
    FOREIGN KEY ([DeliverStop_DStopId])
    REFERENCES [dbo].[DeliverStops]
        ([DStopId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_OrderDeliverStop'
CREATE INDEX [IX_FK_OrderDeliverStop]
ON [dbo].[Orders]
    ([DeliverStop_DStopId]);
GO

-- --------------------------------------------------
-- Script has ended
-- --------------------------------------------------