﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Script.Serialization;

namespace LOP_DatabaseModel.Model
{
    public class OrderMetadata
    {
        
        [ScriptIgnore(ApplyToOverrides = true)]
        public Nullable<int> id { get; set; }

        [Display(Name = "Order Time Stamp")]
        [ScriptIgnore(ApplyToOverrides = true)]
        public string time { get; set; }
        [ScriptIgnore(ApplyToOverrides = true)]

       
        [Display(Name = "Order Number")]
        public Nullable<int> order_number { get; set; }

        [ScriptIgnore(ApplyToOverrides = true)]
        [Display(Name = "Shipment Price")]
        public string price { get; set; }

       
        [Display(Name = "Customer Number")]
        public string customer_number { get; set; }


        [Display(Name = "Cost Centre")]
        public string cost_center { get; set; }


        [ScriptIgnore(ApplyToOverrides = true)]
        [Display(Name = "Final Price")]
        public Nullable<double> final_price { get; set; }

        [ScriptIgnore(ApplyToOverrides = true)]
        public Nullable<int> flagged { get; set; }

        [ScriptIgnore(ApplyToOverrides = true)]
        public Nullable<bool> read { get; set; }

        [ScriptIgnore(ApplyToOverrides = true)]
        public Nullable<bool> pending { get; set; }

        [ScriptIgnore(ApplyToOverrides = true)]
        [Display(Name = "Submit Origin")]
        public string origin { get; set; }

        [ScriptIgnore(ApplyToOverrides = true)]
        [Display(Name = "Status Date")]
        public string status_date { get; set; }

        [Display(Name = "Package Ready Time")]
        public string ready_time { get; set; }


        [ScriptIgnore(ApplyToOverrides = true)]
        [Display(Name = "Deliver By")]
        public string deliver_by { get; set; }

        [ScriptIgnore(ApplyToOverrides = true)]
        [Display(Name = "Dispatch Driver")]
        public string dispatch_driver { get; set; }

        [ScriptIgnore(ApplyToOverrides = true)]
        [Display(Name = "Status Version Code")]
        public string version { get; set; }

        [ScriptIgnore(ApplyToOverrides = true)]
        [Display(Name = "Order Status")]
        public string status { get; set; }

        [ScriptIgnore(ApplyToOverrides = true)]
        [Display(Name = "Order Type")]
        public string order_type { get; set; }

        [ScriptIgnore(ApplyToOverrides = true)]
        public virtual PickupStop PickupStop { get; set; }

        [ScriptIgnore(ApplyToOverrides = true)]
        public virtual DeliverStop DeliverStop { get; set; }
    }

    public class PickupStopMetadata
    {
        [ScriptIgnore(ApplyToOverrides = true)]
        public int PStopId { get; set; }


        [Required]
        [Display(Name = "Company")]
        public string company { get; set; }
       


        [Required]
        [Display(Name = "Address")]
        public string address { get; set; }

        [Required]
        [Display(Name = "Suite")]
        public string suite { get; set; }

        [Required]
        [Display(Name = "City")]
        public string city { get; set; }

        [Required]
        [Display(Name = "Province")]
        public string state { get; set; }

        [Required]
        [Display(Name = "Postal Code")]
        public string postal_code { get; set; }

        [Required]
        [Display(Name = "Country")]
        public string country { get; set; }

        [Display(Name = "Contact Name")]
        public string name { get; set; }

        
        //public string phone { get; set; }
        [Display(Name = "Contact Phone Number")]
        public string phone { get; set; }

        [ScriptIgnore(ApplyToOverrides = true)]
        public virtual Order Order { get; set; }
    }

    public class DeliverStopMetadata
    {

        [ScriptIgnore(ApplyToOverrides = true)]
        public int DStopId { get; set; }



        [Required]
        [Display(Name = "Company")]
        public string company { get; set; }

        [Required]
        [Display(Name = "Address")]
        public string address { get; set; }

        [Required]
        [Display(Name = "Suite")]
        public string suite { get; set; }

        [Required]
        [Display(Name = "City")]
        public string city { get; set; }

        [Required]
        [Display(Name = "Province")]
        public string state { get; set; }

        [Required]
        [Display(Name = "Postal Code")]
        public string postal_code { get; set; }

        [Required]
        [Display(Name = "Country")]
        public string country { get; set; }


        [Display(Name = "Contact Name")]
        public string name { get; set; }
        [Display(Name = "Contact Phone Number")]
        public string phone { get; set; }



        [ScriptIgnore(ApplyToOverrides = true)]
        [Display(Name = "Paper Waybill")]
        public string paper_waybill { get; set; }
       
        [Required]
        [Display(Name = "Special Instructions")]
        public string special_instructions { get; set; }


        [ScriptIgnore(ApplyToOverrides = true)]
        [Display(Name = "Return Address")]
        public string return_add { get; set; }

        [Required]
        [Display(Name = "Service Type")]
        public string service_type { get; set; }

        [Required]
        [Display(Name = "Package Type")]
        public string package { get; set; }

        [Required]
        [Display(Name = "Number of Pieces in Package")]
        public Nullable<int> number_of_pieces { get; set; }

        [Required]
        [Display(Name = "Package Weight")]
        public Nullable<int> weight { get; set; }


        [ScriptIgnore(ApplyToOverrides = true)]
        public string vehicle { get; set; }
        [ScriptIgnore(ApplyToOverrides = true)]
        public string driver_number { get; set; }
        [ScriptIgnore(ApplyToOverrides = true)]
        public string dispatch_message { get; set; }
        [ScriptIgnore(ApplyToOverrides = true)]
        public string notes { get; set; }
        [ScriptIgnore(ApplyToOverrides = true)]
        public string signature_contact { get; set; }
        [ScriptIgnore(ApplyToOverrides = true)]
        [Display(Name = "Reference Number")]
        public string reference { get; set; }
        [ScriptIgnore(ApplyToOverrides = true)]
        public string signature { get; set; }
        [ScriptIgnore(ApplyToOverrides = true)]
        public string signature_lines { get; set; }
        [ScriptIgnore(ApplyToOverrides = true)]
        public string fuel_surcharge { get; set; }
        [ScriptIgnore(ApplyToOverrides = true)]
        public Nullable<int> route_stop_id { get; set; }
        [ScriptIgnore(ApplyToOverrides = true)]
        public string route_status { get; set; }
        [ScriptIgnore(ApplyToOverrides = true)]
        public Nullable<int> distance { get; set; }
        [ScriptIgnore(ApplyToOverrides = true)]
        public Nullable<int> air_distance { get; set; }
        [ScriptIgnore(ApplyToOverrides = true)]
        public string route_status_detail { get; set; }
        [ScriptIgnore(ApplyToOverrides = true)]
        public string driver_pricelist { get; set; }
        [ScriptIgnore(ApplyToOverrides = true)]
        public string route_status_date { get; set; }


        [ScriptIgnore(ApplyToOverrides = true)]
        [Display(Name = "Date Recieved")]
        public string receive_date { get; set; }


        [ScriptIgnore(ApplyToOverrides = true)]
        [Display(Name = "Date Dispatched")]
        public string dispatch_date { get; set; }


        [ScriptIgnore(ApplyToOverrides = true)]
        [Display(Name = "Date Picked-Up")]
        public string pickup_date { get; set; }


        [ScriptIgnore(ApplyToOverrides = true)]
        [Display(Name = "Date Delivered")]
        public string delivery_date { get; set; }

        [ScriptIgnore(ApplyToOverrides = true)]
        [Display(Name = "Date Cancelled")]
        public string cancel_date { get; set; }


        [ScriptIgnore(ApplyToOverrides = true)]
        [Display(Name = "Date Confirmed")]
        public string confirm_date { get; set; }

        [ScriptIgnore(ApplyToOverrides = true)]
        public virtual Order Order { get; set; }
    }

}
