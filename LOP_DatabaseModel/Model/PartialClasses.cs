﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LOP_DatabaseModel.Model
{
    [MetadataType(typeof(OrderMetadata))]
    public partial class Order
    { }

    [MetadataType(typeof(PickupStopMetadata))]
    public partial class PickupStop
    { }

    [MetadataType(typeof(DeliverStopMetadata))]
    public partial class DeliverStop
    { }

}
